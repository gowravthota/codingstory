#imports
import os
import urllib.request
import os
import tempfile
import sys
import fileinput

#write and run
def main():
    f = open("board/temp.txt", 'a+')
    f.write('\n\tf.write(str(main()))')
    f.write('\nexcept Exception as e:')
    f.write('\n\terror = open("error.txt", "w")')
    f.write('\n\terror.write(str(e))')
    f.write('\n\terror.close()')
    f.close()
    os.system('python board/temp.txt')

#check
def check(difficulty, count):
    response = "board/answer.txt"
    answers = "board/"+difficulty+"/answers.txt"
    with open(response) as f:
        response = f.read().splitlines()
    with open(answers) as f:
        answers = f.read().splitlines()
    if(str(response)==str(answers[count])):
        return True
    else:
        return False
