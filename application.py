#imports
import os
import time
import random
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.app import App

import coding

#files
story="board/story.txt"
with open(story) as f:
    lines = f.read().splitlines() 
config="config.txt"
with open(config) as f:
    settings = f.read().splitlines()
error="error.txt"
with open(error) as f:
    error = f.read().splitlines()
#config
start=0
name=""
difficulty=""
for line in settings:
    if(line.find("save")!=-1):
        start=int(line[5:len(line)])
    if(line.find("name")!=-1):
        name=str(line[5:len(line)])
    if(line.find("difficulty")!=-1):
        difficulty=str(line[11:len(line)])
        
#questions and answers
questions="board/"+difficulty+"/questions.txt"
answers="board/"+difficulty+"/answers.txt"
with open(questions) as f:
    questions = f.read().splitlines()
with open(answers) as f:
    answers = f.read().splitlines()

#Main View
class MainView(BoxLayout):
    #class variables
    story_count=0
    problem_count=0
    index=0
    solve=True

    #super
    def __init__(self, **kwargs):
        super(MainView, self).__init__(**kwargs)

        self.main_text = Label()
        self.add_widget(self.main_text)
    
    #update text
    def update(self):
        MainView.story_count+=1
        if(MainView.solve==False):
            MainView.story_count-=1
            pass
        elif(lines[MainView.story_count]=="<>"):
            self.code()
        else:
            #add to text
            result=""
            for x in range(start,MainView.story_count):
                if(lines[x]!="<>"):
                    result+=(lines[x]+"\n")
            self.main_text.text = result

    #on touch
    def on_touch_up(self, touch):
        if self.collide_point(*touch.pos):
            self.update()

    #get problem
    def code(self):
        MainView.solve=False

        #output
        self.output = Label(size_hint =(.5, .25))

        #TextInput
        self.code_problem = TextInput(multiline=True, hint_text="(python 3.7)\ndef main(*args):")
        
        #buttons
        self.run_code = Button(text ='Run',size_hint =(.5, .25))
        self.run_code.bind(on_press=self.save)
        
        #widgets
        self.question = Label(text="Answer the question:\n"+questions[MainView.problem_count], size_hint =(.5, .25))
        self.codebox = BoxLayout(orientation='vertical')
        self.codebox.add_widget(self.question)
        self.codebox.add_widget(self.code_problem)
        self.codebox.add_widget(self.run_code)
        self.codebox.add_widget(self.output)
        self.add_widget(self.codebox)

    #save input code in temp file
    def save(self, event):
        f = open("board/temp.txt", 'w')
        f.write('try:')
        f.write('\n\tf = open("board/answer.txt", "w")\n')
        f.write("\t"+self.code_problem.text)
        f.close()
        coding.main()
        if(coding.check(difficulty, MainView.problem_count)):
            MainView.solve=True;
            MainView.problem_count+=1
            self.remove_widget(self.codebox)

        #output
        self.output = error

class MyApp(App):
    def build(self):
        return MainView()

#Run App
if __name__ == "__main__":
    MyApp().run()
